#!/usr/bin/env bash
cd ~/serveur;
pkill -f node;
pkill -f npm;
git fetch origin;
git checkout testserver;
git merge origin/testserver;
npm install;
node app.js >> ~/server.log 2>&1 &

#also restart web client, just in case
cd ~/web;
npm install;
npm run dev >> ~/web.log 2>&1 &
