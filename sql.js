const logger = require('./logger');
const properties = require('properties-reader')('configuration.properties');

const mysql = require('mysql');
const connection = mysql.createConnection({
    host: properties.path().mysql.host,
    user: properties.path().mysql.user,
    password: properties.path().mysql.password,
    database: properties.path().mysql.dbname,
    port: properties.path().mysql.port
});

connection.connect((err) => {
    if (err) {
        logger.error("Cannot connect to the database: " + err);
        logger.info("Terminating the application.");
    }
});

module.exports = connection;