describe('Test de RoomService', () => {

    const assert = require('assert');
    const chai = require('chai');
    const chaiHttp = require('chai-http');
    const BASE_URL = "http://localhost:8081";
    const expect = chai.expect;
    chai.use(chaiHttp);
    /*const fs = require('fs');
    const _p = require('util').promisify;*/
    const uuidv4 = require("uuid/v4");
    const Room = require("../model/Room.js");
    const dictionary = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
    this.roomCode = "";
    for (let i = 0; i < 6; i++) {
        this.roomCode = this.roomCode.concat(dictionary.charAt((Math.random() * 100000) % dictionary.length));
    }

    // beforeEach('', () => {
    //     console.dir(content);
    // });

    describe('Tests des cas OK', () => {
        describe('TEST CREATE ROOM', () => {

            it(`should create a new Room `, () => {
                return new Promise((resolve, reject) => {

                    let r = new Room();
                    assert.ok(r instanceof Room);
                    resolve();
                });
            });

        });

        describe('TEST GET ROOM', () => {

            it(`should connect to a Room `, () => {
                return new Promise((resolve, reject) => {
                    chai.request(BASE_URL).post('/room').end((err, res) => {
                        chai.request(BASE_URL)
                        .get('/room/' + res.body.roomCode)
                        .end((err, res1) => {
                            expect(res1).to.have.status(200);
                            expect(res1).to.be.json;
                            resolve();
                        });
                    })
                });
            });

        });
    });

    describe('Tests des cas KO - Gestion des erreurs', () => {

        describe('TEST GET', () => {
            it('should failed if room does not exist', () => {
                return new Promise((resolve, reject) => {
                    chai.request(BASE_URL)
                        .get('/room/' + "000000")
                        .end((err, res) => {
                            expect(res).to.have.status(404);
                            resolve();
                        });
                    })
                });
            });


        describe('TEST GET', () => {
            it('should failed if room is full', () => {
                return new Promise((resolve, reject) => {
                    chai.request(BASE_URL).post('/room').end((err1, res) => {
                        for (let i = 0; i < 8; i++) {
                            chai.request(BASE_URL)
                                .get('/room/' + res.body.roomCode)
                                .end((err, res) => {
                                    expect(res).to.have.status(200);
                                });
                        }
                        chai.request(BASE_URL)
                            .get('/room/' + res.body.roomCode)
                            .end((err, res) => {
                                expect(res).to.have.status(403);
                                resolve();
                            })

                    })
                });
            });
        });

    });

    /*after(`Suppression du repertoire ${CONFIG.contentDirectory} s'il est vide`, () => {
        _p(fs.readdir)(CONFIG.contentDirectory)
            .then(files => {
                if (files.length > 0) {
                    return assert.ok(false, `Le répertoire ${CONFIG.contentDirectory} utilisé pour les tests n'est pas vide.`);
                }

                return _p(fs.rmdir)(CONFIG.contentDirectory);
            })
            .catch(err => {
                console.error(err);
            });
    });*/
});