const should = require('should');
const properties = require('properties-reader')('configuration.properties');
const io = require('socket.io-client');
const MESSAGES = require('../game/messages').MESSAGES;
const axios = require('axios');

const socketUrl = `http://${properties.path().server.socket.host}:${properties.path().server.socket.port}`;
const httpUrl = `http://localhost:${properties.path().server.http.port}`;
console.log(httpUrl);
const options = {
    'force new connection': true,
};

function init(callback) {
    // create room
    axios.post(`${httpUrl}/room`).then(response => {
        const hostClientId = response.data.clientId;
        const roomCode = response.data.roomCode;

        // connect host
        const host = io.connect(socketUrl, options);
        host.emit(MESSAGES.HOST_GAME, {clientId: hostClientId});

        // connect players
        axios.get(`${httpUrl}/room/${roomCode}`).then(response0 => {
            const player1ClientId = response0.data.clientId;
            const player1 = io.connect(socketUrl, options);
            player1.emit(MESSAGES.JOIN_GAME, {clientId: player1ClientId, playerName: 'player1'});
            callback(host, hostClientId, player1, player1ClientId);
        });

    });
}

function initAndStart(callback) {
    init(function (host, hostClientId, player, playerClientId) {
        axios.get(`${httpUrl}/theme`).then(response => {
            host.emit(MESSAGES.START_GAME, {
                clientId: hostClientId,
                themes: response.data
            });

            player.on(MESSAGES.GAME_STARTS, function (data) {
                callback(host, hostClientId, player, playerClientId);
            });
        })
    });
}

describe("Game server", function () {
    it('should notify the host when a player joins', function (done) {
        init(function (host, hostClientId, player, playerClientId) {
            host.on(MESSAGES.NEW_PLAYER, function (data) {
                data.playerName.should.equal('player1');
                done();
            });
        });
    });

    it('should notify the players when the game starts', function (done) {
        initAndStart(function (host, hostClientId, player, playerClientId) {
            done();
        });
    });

    it('should send the question to the host', function (done) {
        initAndStart(function (host, hostClientId, player, playerClientId) {
            host.on(MESSAGES.DISPLAY_QUESTION, data => {
               data.question.should.not.be.empty('string');
               data.answers.should.be.instanceOf(Array);
               data.answers.should.not.be.empty('Array');
               data.answers.length.should.be.greaterThan(0);
               data.answers[0].should.not.be.empty('string');
               done();
            });
        });
    });

    it('should ask the players for an answer', function (done) {
       initAndStart(function(host, hostClientId, player, playerClientId) {
           player.on(MESSAGES.ASK_FOR_ANSWER, data => {
              data.answerCount.should.be.greaterThan(0);
              done();
           });
       });
    });

    it('should tell the players whether or not their answer was correct', function (done) {
        initAndStart(function(host, hostClientId, player, playerClientId) {
            player.on(MESSAGES.ASK_FOR_ANSWER, data => {
                player.emit(MESSAGES.GIVE_ANSWER, {
                    clientId: playerClientId,
                    answer: 0
                });
                player.on(MESSAGES.QUESTION_END, data => {
                   data.isAnswerCorrect.should.be.instanceOf(Boolean);
                   done();
                });
            });
        });
    });

    it('should notify the host when a player gives an answer', function(done) {
        initAndStart(function(host, hostClientId, player, playerClientId) {
            player.on(MESSAGES.ASK_FOR_ANSWER, data => {
                player.emit(MESSAGES.GIVE_ANSWER, {
                    clientId: playerClientId,
                    answer: 0
                });
                host.on(MESSAGES.UPDATE_ANSWER_STATUS, data => {
                    data.answerStatuses.should.be.instanceOf(Array);
                    data.answerStatuses.should.not.be.empty('Array');
                    data.answerStatuses[0][0].should.equal(playerClientId);
                    data.answerStatuses[0][1].should.equal(true);
                    done();
                });
            });
        });
    });

    it('should ask a new question after all players are ready', function(done) {
        initAndStart(function(host, hostClientId, player, playerClientId) {
            let askForAnswerCount = 0;
            player.on(MESSAGES.ASK_FOR_ANSWER, data => {
                if (++askForAnswerCount === 2) {
                    done();
                }

                player.emit(MESSAGES.GIVE_ANSWER, {
                    clientId: playerClientId,
                    answer: 0
                });
                player.on(MESSAGES.QUESTION_END, data => {
                   player.emit(MESSAGES.PLAYER_READY, {clientId: playerClientId});
                });
            });
        });
    });

    it('should allow a player to add questions to the current game', function(done) {
        initAndStart(function(host, hostClientId, player, playerClientId) {
            player.on(MESSAGES.ASK_FOR_ANSWER, data => {
                player.emit(MESSAGES.GIVE_ANSWER, {
                    clientId: playerClientId,
                    answer: 0
                });
                player.on(MESSAGES.QUESTION_END, data => {
                    player.emit(MESSAGES.ADD_QUESTION, {
                       clientId: playerClientId,
                       title: "Question",
                       answers: [
                           {
                               title: "Réponse 1",
                               correct: true,
                           },
                           {
                               title: "Réponse 2",
                               correct: false,
                           },
                           {
                               title: "Réponse 1",
                               correct: false,
                           },
                           {
                               title: "Réponse 1",
                               correct: false,
                           },
                       ]
                    });
                    done();
                });
            });
        });
    });

    it('should send the leaderboard to the host between questions', function(done) {
        initAndStart(function(host, hostClientId, player, playerClientId) {
            player.on(MESSAGES.ASK_FOR_ANSWER, data => {
                player.emit(MESSAGES.GIVE_ANSWER, {
                    clientId: playerClientId,
                    answer: 0
                });

                host.on(MESSAGES.SHOW_LEADERBOARD, data => {
                    data.correctAnswer.should.be.greaterThanOrEqual(0);
                    data.scores.should.be.an.instanceOf(Array);
                    data.scores.length.should.equal(1);
                    data.scores[0].clientId.should.equal(playerClientId);
                    data.scores[0].score.should.be.greaterThanOrEqual(0);
                    done();
                });
            });
        });
    });
});
