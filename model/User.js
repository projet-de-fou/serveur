const logger = require("../logger");

module.exports = class {

    constructor(id, mail, password, userName, avatar=null, connected=false) {
        this.id = id;
        this.mail = mail;
        this.password = password;
        this.userName = userName;
        this.avatar = avatar;
        this.connected = connected;
    };

};