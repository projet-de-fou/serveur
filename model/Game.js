const exceptions = require("../exceptions");
const Room = require("./Room");
const Client = require("./Client");

module.exports = class {

    constructor(room) {
        this.room = room;
        this.host = undefined;
        this.players = [];
        this.answers = [];
        this.scores = [];
        this.playersReady = [];
        this.themes = [];
        this.questions = [];
        this.currentQuestion = undefined;
    }

    connectHost(socket, clientId) {
        if (this.room.hostId === clientId) {
            this.host = new Client(socket, clientId, undefined);
        } else {
            throw exceptions.functionalException(exceptions.FUNCTIONAL_EXCEPTION.BAD_CLIENT_ID);
        }
    }

    nextQuestion() {
        const question = this.questions.shift();
        this.currentQuestion = question;
        return question;
    }

    connectPlayer(socket, playerClientId, username) {
        this.players.push(new Client(socket, playerClientId, username));
        this.scores.push(0);
        this.playersReady.push(false);
    }

    sendMessageToHost(type, message) {
        this.host.socket.emit(type, message);
    }

    sendMessageToPlayers(type, message) {
        this.players.forEach(player => player.socket.emit(type, message));
    }

    sendPersonalizedMessageToPlayers(type, messageBodyFunction) {
        this.players.forEach(player => player.socket.emit(type, messageBodyFunction(player)));
    }

    setPlayerAnswer(clientId, answer) {
        const playerIndex = this.players.indexOf(this.players.find(player => player.clientId === clientId));
        this.answers[playerIndex] = answer;
        if (this.currentQuestion.getCorrectAnswerIndex().toString() === answer.toString()) {
            this.scores[playerIndex] += 100;
        }
    }

    getAnswerStatus() {
        return this.players.map(player => [
            player.clientId,
            this.answers[this.players.indexOf(player)] !== undefined
        ]);
    }

    isAnswerCorrect(player) {
        return parseInt(this.answers[this.players.indexOf(player)]) === parseInt(this.currentQuestion.getCorrectAnswerIndex());
    }

    allPlayersAnswered() {
        let answerCount = 0;
        for (let i in this.answers) {
            if (this.answers[i] !== undefined) {
                answerCount++;
            }
        }
        return answerCount === this.players.length;
    }

    allPlayersReady() {
        for (let i in this.playersReady) {
            if (this.playersReady[i] === false) {
                return false;
            }
        }

        return true;
    }

    getCorrectAnswerIndex() {
        return this.currentQuestion.getCorrectAnswerIndex();
    }

    getLeaderboard() {
        const leaderboard = [];
        for (let i in this.players) {
            leaderboard.push({
                clientId: this.players[i].clientId,
                score: this.scores[i]
            });
        }
        return leaderboard;
    }

    waitForPlayers() {
        for (let i in this.playersReady) {
            this.playersReady[i] = false;
        }
    }

    setPlayerReady(clientId) {
        for (let i in this.players) {
            if (this.players[i].clientId === clientId) {
                this.playersReady[i] = true;
                return;
            }
        }
    }

    resetPlayersReady() {
        for (let i in this.playersReady) {
            this.playersReady[i] = false;
        }
    }

    resetAnswers() {
        this.answers = [];
    }

    queueQuestion(question) {
        const randomIndex = Math.floor(Math.random()*this.questions.length);
        this.questions.splice(randomIndex, 0, question);
    }

    end() {
        this.host.socket.disconnect(true);
        this.players.forEach(player => player.socket.disconnect(true));
    }

    getResults() {
        let results = [];

        for(let i in this.scores) {
            results.push({
                playerClientId: this.players[i].clientId,
                username: this.players[i].userName,
                score: this.scores[i],
                rank: undefined
            });
        }

        results.sort((a, b) => b.score - a.score);

        for (let i in results) {
            // I have to parse to int here because javascript is stupid
            // and considers i to be a string
            results[i].rank = parseInt(i) + 1;
        }

        return results;
    }
};
