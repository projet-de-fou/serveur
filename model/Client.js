module.exports = class {

    constructor(socket, clientId, userName) {
        this.socket = socket;
        this.clientId = clientId;
        this.userName = userName;
    }
};