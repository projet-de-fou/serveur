module.exports = class {

    constructor(id, title, themeId, isHistory, userId, answers) {
        this.id = id;
        this.title = title;
        this.themeId = themeId;
        this.isHistory = isHistory;
        this.userId = userId;
        this.answers = answers;
    }

    getCorrectAnswerIndex() {
        for (let i in this.answers) {
            if (this.answers[i].isGoodAnswer) {
                return i;
            }
        }
    }
};
