module.exports = class {

    constructor(id, title, questionId, isGoodAnswer) {
        this.id = id;
        this.title = title;
        this.questionId = questionId;
        this.isGoodAnswer = isGoodAnswer;
    }
};
