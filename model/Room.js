const uuidv4 = require("uuid/v4");
const exceptions = require("../exceptions");

module.exports = class {
    constructor() {
        const dictionary = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        this.roomCode = "";
        for (let i = 0; i < 6; i++) {
            this.roomCode = this.roomCode.concat(dictionary.charAt((Math.random() * 100000) % dictionary.length));
        }

        this.hostId = uuidv4();
        this.playerIdList = [];
    }

    get playerCount() {
        return this.playerIdList.length;
    }

    isFull() {
        return this.playerCount === 8;
    }

    registerNewPlayer() {
        if (this.isFull()) {
            throw new exceptions.functionalException(exceptions.FUNCTIONAL_EXCEPTION.ROOM_IS_FULL);
        }

        const playerId = uuidv4();
        this.playerIdList.push(playerId);
        return playerId;
    }
};