module.exports.FUNCTIONAL_EXCEPTION = Object.freeze({
    "ROOM_IS_FULL": 101,
    "ROOM_DOES_NOT_EXIST": 102,
    "BAD_CLIENT_ID": 201,
    "INVALID_CREDENTIALS": 301,
    "USER_NOT_FOUND": 302,
    "USER_ALREADY_CONNECTED": 303,
    "USER_ALREADY_EXISTS": 304,
    "INCORRECT_MAIL_ADDRESS": 305,
    "INCORRECT_PASSWORD": 306,
    "INCORRECT_USERNAME": 307,
    "TOO_MUCH_CHARACTERS": 401,
    "QUESTION_NOT_FOUND": 402,
    "ANSWERS_NOT_FOUND": 501,
});

module.exports.functionalException = function(code) {
    this.code = code;
};

module.exports.TECHNICAL_EXCEPTION = Object.freeze({
    "SQL_ERROR": 101,
});

module.exports.technicalException = function(code, message) {
    this.code = code;
    this.message = message;
};

