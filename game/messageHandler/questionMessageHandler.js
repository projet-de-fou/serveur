const logger = require("../../logger");
const messages = require("../messages");
const GameService = require("../../service/GameService")();

module.exports = function() {
    return {
        handleGiveAnswer: function(socket, data) {
            logger.debug("Player gave an answer");
            GameService.giveAnswer(data.clientId, data.answer, game => {
                game.sendMessageToHost(messages.MESSAGES.UPDATE_ANSWER_STATUS, { answerStatuses: game.getAnswerStatus()});
                if (game.allPlayersAnswered()) {
                        game.sendMessageToHost(messages.MESSAGES.SHOW_LEADERBOARD, {
                            scores: game.getLeaderboard(),
                            correctAnswer: game.getCorrectAnswerIndex()
                        });

                        game.sendPersonalizedMessageToPlayers(messages.MESSAGES.QUESTION_END, function(player){
                            return {
                                isAnswerCorrect: game.isAnswerCorrect(player)
                            }
                        });
                        game.resetAnswers();
                        game.waitForPlayers();
                }
            });
        },
        handleAddQuestion: function (socket, data) {
            GameService.addQuestion(data.clientId, data.title, data.answers);
        }
    }
};
