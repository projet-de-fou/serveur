const exception = require("../../exceptions");
const GameService = require("../../service/GameService")();
const logger = require("../../logger");
const messages = require("../messages");
const configuration = require('properties-reader')('configuration.properties');

module.exports = function() {
    return {
        handleHostGame: function(socket, data) {
            logger.debug("client " + data.clientId + " is trying to host a game");
            try {
                GameService.hostGame(socket, data.clientId);
            } catch(e) {
                if (e instanceof exception.functionalException) {
                    logger.error("Functional exception during handleHostGame: " + e.code);
                } else {
                    logger.error("Unknown exception during handleHostGame: " + e);
                }
            }
        },
        handleJoinGame: function(socket, data) {
            logger.trace(data);
            logger.debug("client " + data.clientId + " is trying to join a game");
            try {
                const game = GameService.joinGame(socket, data.clientId, data.playerName);
                game.sendMessageToHost(messages.MESSAGES.NEW_PLAYER, {
                    playerClientId: data.clientId,
                    playerName: data.playerName
                });

            } catch (e) {
                if (e instanceof exception.functionalException) {
                    logger.error("Functional exception during handleJoinGame: " + e.code);
                } else {
                    logger.error("Unknown exception during handleJoinGame: " + e);
                }
            }
        },
        handleStartGame: function(socket, data) {
            logger.trace(data);
            logger.debug("client " + data.clientId + " is trying to start a game with themes : " + data.themes);
            try {
                const mode = configuration.path().mode;
                GameService.startGame(socket, data.clientId, data.themes,
                        game => {
                    game.sendMessageToPlayers(messages.MESSAGES.GAME_STARTS, {});
                    setTimeout(() => {
                        GameService.nextQuestion(game);
                    }, mode !== 'test' ? 3000 : 0);
                });
            } catch (e) {
                if (e instanceof exception.functionalException) {
                    logger.error("Functional exception during handleStartGame: " + e.code);
                } else {
                    logger.error("Unknown exception during handleStartGame: " + e);
                }
            }
        }

    }
};
