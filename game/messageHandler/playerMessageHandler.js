const GameService = require("../../service/GameService")();
const logger = require("../../logger");

module.exports = function() {
    return {
        handlePlayerReady: function(socket, data) {
            logger.debug("Player " + data.userId + " is ready");
            GameService.setPlayerReady(data.clientId);
        }
    }
};