module.exports.MESSAGES = Object.freeze({
    "HOST_GAME": "hostGame",
    "JOIN_GAME": "joinGame",
    "START_GAME": "startGame",
    "GAME_STARTS": "gameStarts",
    "DISPLAY_QUESTION": "displayQuestion",
    "ASK_FOR_ANSWER": "askForAnswer",
    "ANSWER_QUESTION": "answerQuestion",
    "UPDATE_ANSWERS": "updateAnswer",
    "QUESTION_END": "questionEnd",
    "SHOW_LEADERBOARD": "showLeaderboard",
    "PLAYER_READY": "playerReady",
    "ALL_PLAYERS_READY": "allPlayersReady",
    "NEW_PLAYER": "newPlayer",
    "GIVE_ANSWER": "giveAnswer",
    "UPDATE_ANSWER_STATUS": "updateAnswerStatus",
    "END_GAME_HOST": "endGameHost",
    "END_GAME_PLAYER": "endGamePlayer",
    "ADD_QUESTION": "addQuestion",
});

module.exports.message = function(name) {
    this.name = name;
};

