const logger = require("../logger");
const lobbyMessageHandler = require('./messageHandler/lobbyMessageHandler')();
const playerMessageHandler = require('./messageHandler/playerMessageHandler')();
const questionMessageHandler = require('./messageHandler/questionMessageHandler')();
const messages = require('./messages');

module.exports.dispatch = function(socket) {
    const address = socket.request.connection._peername;
    logger.debug("New connection from " + address.address + ":" + address.port);

    // LOBBY DISPATCHER
    socket.on(messages.MESSAGES.HOST_GAME, function(data) {
        logger.trace("Message: " + data);
        lobbyMessageHandler.handleHostGame(socket, data);
    });
    socket.on(messages.MESSAGES.JOIN_GAME, function(data) {
        if ((typeof data) === "string") {
           data = JSON.parse(data);
        }
        logger.trace("Message: " + data);
        lobbyMessageHandler.handleJoinGame(socket, data);
    });
    socket.on(messages.MESSAGES.START_GAME, function(data) {
        logger.trace("Message: " + data);
        lobbyMessageHandler.handleStartGame(socket, data);
    });

    // GAME DISPATCHER
    socket.on(messages.MESSAGES.PLAYER_READY, function(data) {
        if ((typeof data) === "string") {
            data = JSON.parse(data);
        }
        logger.trace("Message: " + data);
        playerMessageHandler.handlePlayerReady(socket, data);
    });
    socket.on(messages.MESSAGES.ALL_PLAYERS_READY, function(data) {
        //data = JSON.parse(data);
        logger.trace("Message: " + data);
        playerMessageHandler.handleAllPlayersReady(socket, data);
    });

    // QUESTION DISPATCHER
    socket.on(messages.MESSAGES.SHOW_QUESTION, function(data) {
        //data = JSON.parse(data);
        logger.trace("Message: " + data);
        questionMessageHandler.handleShowQuestion(socket, data);
    });

    socket.on(messages.MESSAGES.GIVE_ANSWER, function(data) {
        if ((typeof data) === "string") {
            data = JSON.parse(data);
        }
        questionMessageHandler.handleGiveAnswer(socket, data);
    });

    socket.on(messages.MESSAGES.ADD_QUESTION, function(data) {
       if ((typeof data) === "string") {
           data = JSON.parse(data);
       }
       questionMessageHandler.handleAddQuestion(socket, data);
    });
};
