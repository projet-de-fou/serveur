# Serveur

Serveur back Node.js du projet de fou

## Installation

Clonez ce git.
Avant d'installer les dépendences, installez les outils de build pour pouvoir compiler <i>bcrypt</i> en vous référant à la page suivante : https://github.com/kelektiv/node.bcrypt.js/wiki/Installation-Instructions

Note: pour Windows, installez la version 4.0.0 de windows-build-tools <b>avec les droits d'administrateurs</b> :

`npm install --global --production windows-build-tools@4.0.0`

Modifiez ensuite le fichier `configuration.properties` avec le nom d'hôte et le port que vous souhaitez utiliser.

Modifiez le fichier `app.js` pour autoriser les requêtes cross-origin en spécifiant l'url du client hôte.

Il suffit ensuite d'installer les dépendences puis de lancer le serveur :

```
npm install
npm start
```
