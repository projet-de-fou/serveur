const properties = require('properties-reader')('configuration.properties');

const logger = require('simple-node-logger').createSimpleLogger();
logger.setLevel('trace');
module.exports = logger;
