const logger = require('../logger');
const exceptions = require('../exceptions');
const sql = require('../sql');
const Answer = require('../model/Answer');

module.exports = {
    findAll: function (callback) {
        sql.query('SELECT * FROM answer', (err, rows) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in AnswerRepository.findAll()'
                );
            }
            callback(rows.map(row => new Answer(row.id, row.title, row.questionId, row.isGoodAnswer)));
        });
    },
    findById: function (answerId, callback) {
        sql.query('SELECT * FROM answer WHERE id = ?', [answerId], (err, rows) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in AnswerRepository.findById()'
                );
            }
            callback(new Answer(rows[0].id, rows[0].title, rows[0].questionId, rows[0].isGoodAnswer));
        });
    },
    findByQuestionId: function (questionId, callback) {
        sql.query('SELECT * FROM answer WHERE questionId = ?', [questionId], (err, rows) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in AnswerRepository.findById()'
                );
            }
            callback(rows.map(row => new Answer(row.id, row.title, row.questionId, row.isGoodAnswer)));
        });
    },
    save: function (answers, callback) {
        logger.trace("Answers to insert : " + JSON.stringify(answers));
        const request = "INSERT INTO answer(title, questionId, isGoodAnswer) VALUES ?";
        const params = [];
        answers.forEach((answer) => {
            params.push([answer.title, answer.questionId, answer.isGoodAnswer])
        });
        sql.query(request, [params], (err, res) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in AnswerRepository.save()'
                );
            } else callback(true);
        })
    },
    delete: function (id, callback) {
        logger.trace("AnswerId to delete : " + JSON.stringify(id));
        const request = "DELETE FROM answer WHERE id = ?;";
        const params = [id];
        sql.query(request, [params], (err, res) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in AnswerRepository.delete()'
                );
            } else {
                if (res.rowsAffected > 0) callback(true);
                else callback(false);
            }
        })
    },
    deleteByQuestionId: function (questionId, callback) {
        logger.trace("QuestionId to delete : " + JSON.stringify(questionId));
        const request = "DELETE FROM answer WHERE questionId = ?;";
        const params = [questionId];
        sql.query(request, [params], (err, res) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in AnswerRepository.delete()'
                );
            } else {
                if (res.rowsAffected > 0) callback(true);
                else callback(exceptions.FUNCTIONAL_EXCEPTION.ANSWERS_NOT_FOUND);
            }
        })
    }
};
