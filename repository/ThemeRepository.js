const logger = require('../logger');
const exceptions = require('../exceptions');
const sql = require('../sql');
const Theme = require('../model/Theme');

module.exports = {
    findAll: function (callback) {
        sql.query('SELECT * FROM theme', (err, rows) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in ThemeRepository.findAll()'
                );
            }

            callback(rows.map(row => new Theme(row.id, row.name)));
        });
    },

    findByNames: function(names, callback) {
        sql.query('SELECT * FROM theme WHERE theme.name IN (?)', [names], (err, rows) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in ThemeRepository.findByNames()'
                );
            }

            callback(rows.map(row => new Theme(row.id, row.name)));
        });
    },
    findById: function (themeId, callback) {
        sql.query('SELECT * FROM theme WHERE id = ?', [themeId], (err, rows) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in ThemeRepository.findById()'
                );
            }

            callback(new Theme(rows[0].id, rows[0].name));
        });
    }
};
