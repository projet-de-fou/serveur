const logger = require('../logger');
const exceptions = require('../exceptions');
const sql = require('../sql');
const Question = require('../model/Question');
const Answer = require('../model/Answer');
const AnswerRepository = require('./AnswerRepository');

module.exports = {
    findAll: function (callback) {
        sql.query('SELECT * FROM question', (err, rows) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in QuestionRepository.findAll()'
                );
            }

            callback(rows.map(row => new Question(row.id, row.title, row.themeId)));
        });
    },
    findById: function (questionId, callback) {
        sql.query('SELECT * FROM question WHERE id = ?', [questionId], (err, rows) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in QuestionRepository.findById()'
                );
            }

            callback(new Question(rows[0].id, rows[0].title, rows[0].themeId));
        });
    },
    findByTheme: function (themeId, callback) {
        sql.query('SELECT * FROM question WHERE themeId = ?', [themeId], (err, rows) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in QuestionRepository.findByThemeId()'
                );
            }

            callback(rows.map(row => new Question(row.id, row.title, row.themeId)));
        });
    },
    findByThemeTitle: function (themeId, callback) {
        sql.query('SELECT * FROM question WHERE title = ?', [themeId], (err, rows) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in QuestionRepository.findByThemeId()'
                );
            }

            callback(rows.map(row => new Question(row.id, row.title, row.themeId)));
        });
    },
    save: function (question, callback) {
        logger.trace("Question to insert : " + JSON.stringify(question));
        const request = "INSERT INTO question(title, themeId, isHistory, userId) VALUES ?";
        const params = [[question.title, question.themeId, question.isHistory, question.userId]];
        sql.query(request, [params], (err, res) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in QuestionRepository.save()'
                );
            } else  callback(res);
        })
    },
    delete: function (id, callback) {
        logger.trace("QuestionId to delete : " + JSON.stringify(id));
        const request = "DELETE FROM answer WHERE id = ?;";
        const params = [id];
        sql.query(request, [params], (err, res) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in QuestionRepository.delete()'
                );
            } else {
                if (res.rowsAffected > 0) {
                    AnswerRepository.deleteByQuestionId(id, (result) => {
                            callback(result)
                    });
                }
                else callback(exceptions.FUNCTIONAL_EXCEPTION.QUESTION_NOT_FOUND);
            }
        })
    },
    findByThemesWithAnswers: function (themeIds, callback) {
        console.log(`themeIds: ${themeIds}`);
        sql.query(`SELECT question.id         as questionId,
                          question.title      as questionTitle,
                          question.themeId    as themeId,
                          answer.id           as answerId,
                          answer.title        as answerTitle,
                          answer.isGoodAnswer as isGoodAnswer,
                          question.isHistory  as isHistory
                   FROM question
                          INNER JOIN answer ON answer.questionId = question.id
                   WHERE question.themeId IN (?) AND isHistory=0`,
            [themeIds], (err, rows) => {
                if (err) {
                    throw new exceptions.technicalException(
                        exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                        'Error when executing sql query in QuestionRepository.findByThemesWithAnswer(): ' + err
                    );
                }
                const questions = [];
                for (const questionRow of rows) {
                    let question = questions.find(q => q.id === questionRow.questionId);
                    if (question === undefined) {
                        question = new Question(questionRow.questionId, questionRow.questionTitle,
                            questionRow.themeId, 0, null, []);
                        questions.push(question);
                    }
                    question.answers.push(new Answer(questionRow.answerId, questionRow.answerTitle,
                            questionRow.questionId, questionRow.isGoodAnswer));
                }

                logger.trace(questions);
                callback(questions);
            });
    }
};
