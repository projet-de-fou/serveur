const logger = require('../logger');
const exceptions = require('../exceptions');
const sql = require('../sql');
const User = require('../model/User');

module.exports = {
    findAll: function (callback) {
        sql.query('SELECT * FROM user', (err, rows) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in UserRepository.findAll()'
                );
            }
            callback(rows.map(row => new User(row.id, row.mail, "***********", row.userName)));
        });
    },
    findById: function (id, callback) {
        sql.query('SELECT * FROM user WHERE id = ?', [id], (err, rows) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in UserRepository.findById()'
                );
            }
            callback(new User(rows[0].id, rows[0].mail, "***********", rows[0].userName));
        });
    },
    findByUserName: function (userName, callback) {
        sql.query('SELECT * FROM user WHERE userName = ?', [userName], (err, rows) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in UserRepository.findByUserName()'
                );
            }
            callback(new User(rows[0].id, rows[0].mail, "***********", rows[0].userName));
        });
    },

    findByMail: function (mail, callback) {
        sql.query('SELECT * FROM user WHERE mail = ?', [mail], (err, rows) => {
            if (rows.length === 0) {
                callback(undefined);
            }  else {
                callback(new User(rows[0].id, rows[0].mail, rows[0].password, rows[0].userName));
            }
        });
    },
    save: function (user, callback) {
        logger.trace("user to insert : " + JSON.stringify(user));
        const request = "INSERT INTO user(userName, password, mail) VALUES ?";
        const params = [[user.userName, user.password, user.mail]];
        sql.query(request, [params], (err, res) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in UserRepository.save()'
                );
            } else callback(true);
        })
    },
    update: function (user, callback) {
        logger.trace("user to update : " + JSON.stringify(user));
        const request = "UPDATE user SET userName = ?, password = ?, mail = ? WHERE id = ?;";
        const params = [user.userName, user.password, user.mail, user.id];
        sql.query(request, params, (err, res) => {
            if (err) {
                throw new exceptions.technicalException(
                    exceptions.TECHNICAL_EXCEPTION.SQL_ERROR,
                    'Error when executing sql query in UserRepository.update()'
                );
            } else callback(true);
        })
    }
};
