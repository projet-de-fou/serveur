const logger = require('../logger');
const ThemeService = require('../service/ThemeService')();
const QuestionService = require('../service/QuestionService')();
const AnswerService = require('../service/AnswerService')();
const UserService = require('../service/UserService')();

const express = require('express');
const app = express();

// THEME CALLS

app.get("/theme", function(req, res) {
    try {
        ThemeService.findAll(themes => {
            return res.status(200).send(themes.map(theme => theme.name));
        });
    } catch (e) {
        logger.error('Error during GET /theme : ' + e);
        return res.status(500).send("Internal server error");
    }
});



// QUESTION CALLS

app.get("/question", function(req, res) {
    try {
        QuestionService.findAll(questions => {
            return res.status(200).send(questions);
        });
    } catch (e) {
        logger.error('Error during GET /question : ' + e);
        return res.status(500).send("Internal server error");
    }
});

app.get("/question/:id", function(req, res) {
    try {
        QuestionService.findById(req.params.id, question => {
            return res.status(200).send(question);
        })
    } catch (e) {
        logger.error('Error during GET /question/id : ' + e);
        return res.status(404).send("Id not found !");
    }
});

app.get("/question/theme/:themeId", function(req, res) {
    try {
        QuestionService.findByTheme(req.params.themeId, question => {
            return res.status(200).send(question);
        })
    } catch (e) {
        logger.error('Error during GET /question/theme/themeId : ' + e);
        return res.status(404).send("Id not found !");
    }
});





// ANSWER CALLS

app.get("/answer", function(req, res) {
    try {
        AnswerService.findAll(answers => {
            return res.status(200).send(answers);
        });
    } catch (e) {
        logger.error('Error during GET /answer : ' + e);
        return res.status(500).send("Internal server error");
    }
});

app.get("/answer/:themeId", function(req, res) {
    try {
        AnswerService.findByThemeId(req.params.themeId, answers => {
            return res.status(200).send(answers);
        })
    } catch (e) {
        logger.error('Error during GET /answer/themeId : ' + e);
        return res.status(404).send("Id not found !");
    }
});





// USER CALLS

app.get("/user", function(req, res) {
    try {
        UserService.findAll(users => {
            return res.status(200).send(users);
        });
    } catch (e) {
        logger.error('Error during GET /user : ' + e);
        return res.status(500).send("Internal server error");
    }
});

app.get("/user/:id", function(req, res) {
    try {
        UserService.findById(req.params.id, users => {
            return res.status(200).send(users);
        })
    } catch (e) {
        logger.error('Error during GET /user/:id : ' + e);
        return res.status(404).send("Id not found !");
    }
});

app.get("/userName/:userName", function(req, res) {
    try {
        UserService.findByUserName(req.params.userName, users => {
            return res.status(200).send(users);
        })
    } catch (e) {
        logger.error('Error during GET /userName/:userName : ' + e);
        return res.status(404).send("UserName not found !");
    }
});

app.get("/mail/:mail", function(req, res) {
    try {
        UserService.findByMail(req.params.mail, users => {
            return res.status(200).send(users);
        })
    } catch (e) {
        logger.error('Error during GET /userName/:userName : ' + e);
        return res.status(404).send("Mail not found !");
    }
});


module.exports = app;