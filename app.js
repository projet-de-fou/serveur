const logger = require('./logger');
const express = require('express');
const exceptions = require('./exceptions');
const path = require('path');
const serveStatic = require('serve-static');
const bodyParser = require('body-parser');
const RoomService = require('./service/RoomService')();
const UserService = require('./service/UserService')();
const AnswerService = require('./service/AnswerService')();
const QuestionService = require('./service/QuestionService')();
const User = require('./model/User');
const dispatch = require('./game/dispatcher').dispatch;
const configuration = require('properties-reader')('configuration.properties');
let sqlReq = require('./routes/sqlReq');
const socketIoServer = require('socket.io')(configuration.path().server.socket.port, {
    path: '/',
    serveClient: false
});
//const cors = require('cors');
const app = express();
//app.use(cors());
app.use(function (req, res, next) {

    res.header('Access-Control-Allow-Origin', 'http://rochdion.net');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});
app.use(serveStatic(path.join(__dirname, 'public-static')));
app.use(bodyParser.json());

app.get("/room/:roomCode", function(req, res) {
    // todo handle game has begun errors
    logger.info("GET /room/" + req.params.roomCode);
    try {
        const playerId = RoomService.joinRoom(req.params.roomCode);
        logger.trace("get /room/xxxx: playerId = " + playerId);
        return res.status(200).send({
            host: configuration.path().server.socket.host,
            port: configuration.path().server.socket.port,
            clientId: playerId
        });
    } catch (e) {
        if (!(e instanceof exceptions.functionalException)) {
            logger.error("Unknown exception: " + e);
            return res.status(500).send("Internal server error");
        }
        switch (e.code) {
            case exceptions.FUNCTIONAL_EXCEPTION.ROOM_DOES_NOT_EXIST:
                return res.status(404).send("Room not found");
            case exceptions.FUNCTIONAL_EXCEPTION.ROOM_IS_FULL:
                return res.status(403).send("Room is full");
            default:
                return res.status(500).send("Internal server error");
        }
    }
});

app.post("/room", function(req, res) {
    const room = RoomService.createRoom();
    return res.status(200).send({
        roomCode: room.roomCode,
        host: configuration.path().server.socket.host,
        port: configuration.path().server.socket.port,
        clientId: room.hostId
    });
});

app.get("/user", function (req, res) {
    logger.info("GET /user");
    UserService.connectUser(req.query.mail, req.query.password, (result) => {
        if (result instanceof User) {
            return res.status(200).send({
                id: result.id,
                mail : result.mail,
                userName: result.userName,
                connected: result.connected
            });
        } else {
            switch (result) {
                case exceptions.FUNCTIONAL_EXCEPTION.USER_NOT_FOUND :
                    return res.status(403).send("User not found");
                case exceptions.FUNCTIONAL_EXCEPTION.INVALID_CREDENTIALS :
                    return res.status(404).send("Invalid credentials");
                case exceptions.FUNCTIONAL_EXCEPTION.USER_ALREADY_CONNECTED :
                    return res.status(404).send("User already connected");
            }
        }
    });
});

app.post("/user", function (req, res) {
    logger.info("POST /user : " + JSON.stringify(req.query));
    UserService.signUp( req.query.mail, req.query.password, req.query.userName, null, (result) => {
        if (!result) {
            switch (result) {
                case exceptions.FUNCTIONAL_EXCEPTION.USER_ALREADY_EXISTS :
                    return res.status(403).send("Email already used for an other account !");
                case exceptions.FUNCTIONAL_EXCEPTION.INVALID_CREDENTIALS :
                    return res.status(404).send("Invalid credentials (Username's length less than 20 characters) !");
            }
        }
        else return res.status(200).send("User created");
    });
});

app.post("/signout", function (req, res) {
    logger.info("POST /signout id= " + req.query.id);
    UserService.disconnect(req.query.id, (result) => {
        if (result === exceptions.FUNCTIONAL_EXCEPTION.USER_NOT_FOUND) return res.status(403).send("User not found or not connected");
        else return res.status(200).send("User disconnected successfully !");
    })
});

app.post("/question", function (req, res) {
   logger.info("POST /question");
   QuestionService.save(req.body.title, req.body.themeId, req.body.answers, (result) => {
       if (result === exceptions.FUNCTIONAL_EXCEPTION.TOO_MUCH_CHARACTERS) return res.status(403).send("Too much characters !");
       else return res.status(200).send("Question created successfully !");
   })
});

app.put("/user", function (req, res) {
    logger.info("PUT /user req : : " + JSON.stringify(req.query));
    UserService.updateUser(req.query.id, req.query.mail, req.query.oldPassword, req.query.newPassword, req.query.userName, null, (result) => {
        if (result === exceptions.FUNCTIONAL_EXCEPTION.USER_NOT_FOUND) return res.status(404).send("User not found !");
        if (result === exceptions.FUNCTIONAL_EXCEPTION.INCORRECT_USERNAME) return res.status(403).send("Username is incorrect (20 characters max) !");
        if (result === exceptions.FUNCTIONAL_EXCEPTION.INCORRECT_MAIL_ADDRESS) return res.status(403).send("Mail address is incorrect !");
        if (result === exceptions.FUNCTIONAL_EXCEPTION.INCORRECT_PASSWORD) return res.status(403).send("Old password is incorrect !");
        else return res.status(200).send("User updated successfully !");
    });
});
app.delete("/question/:id", function (req, res) {
    logger.info("DELETE /question");
    AnswerService.delete(req.params.id, (result) => {
        if (result === exceptions.FUNCTIONAL_EXCEPTION.QUESTION_NOT_FOUND) return res.status(404).send("Question not found !");
        if (result === exceptions.FUNCTIONAL_EXCEPTION.ANSWERS_NOT_FOUND) return res.status(404).send("Answer not found !");
        else return res.status(200).send("Answer deleted successfully !");
    });
});
app.put("/questionHistory", function (req, res) {
    logger.info("PUT /questionHistory : userId:  " + JSON.stringify(JSON.parse(req.body.question)));
    QuestionService.save(JSON.parse(req.body.question).title, JSON.parse(req.body.question).theme, JSON.parse(req.body.question).answers, parseInt(req.body.userId), 1,(result) => {
        if (result === exceptions.FUNCTIONAL_EXCEPTION.INVALID_CREDENTIALS) return res.status(403).send("At least one of the fields is incorrect !");
        else return res.status(200).send("User updated successfully !");
    });
});

app.use(sqlReq);
app.listen(configuration.path().server.http.port);
socketIoServer.on('connection', dispatch);

