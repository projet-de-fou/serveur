const Room = require("../model/Room");
const exceptions = require("../exceptions");

module.exports = function () {
    this.rooms = [];

    return {
        findRoomByCode: function (roomCode) {
            return rooms.find(function (room) {
                return room.roomCode === roomCode;
            });
        },
        findRoomByHostClientId: function (clientId) {
            return rooms.find(function (room) {
               return room.hostId === clientId;
            });
        },
        roomExists: function (roomCode) {
            return this.findRoomByCode(roomCode) !== undefined;
        },
        joinRoom: function (roomCode) {
            const room = this.findRoomByCode(roomCode);
            if (room === undefined) {
                throw new exceptions.functionalException(exceptions.FUNCTIONAL_EXCEPTION.ROOM_DOES_NOT_EXIST);
            }

            return room.registerNewPlayer();
        },
        createRoom: function () {
            let room = new Room();
            rooms.push(room);
            return room;
        }
    };
};