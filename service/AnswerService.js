const logger = require("../logger");
const exceptions = require("../exceptions");
const AnswerRepository = require("../repository/AnswerRepository");

module.exports = function () {
    return {
        findAll: function(callback) {
            AnswerRepository.findAll(callback);
        },
        findById: function(id, callback) {
            AnswerRepository.findById(id, callback);
        },
        findByThemeId: function (themeId, callback) {
            AnswerRepository.findByQuestionId(themeId, callback);
        },
        delete: function (id, callback) {
            AnswerRepository.delete(id, (res) => {
                callback(res);
            });

        }
    };
};
