const logger = require("../logger");
const exceptions = require("../exceptions");
const QuestionRepository = require("../repository/QuestionRepository");
const Question = require("../model/Question");
const AnswerRepository = require("../repository/AnswerRepository");
const Answer = require("../model/Answer");
const ThemeService = require("./ThemeService")();
const shuffle = require('shuffle-array');

module.exports = function () {
    return {
        findAll: function(callback) {
            QuestionRepository.findAll(callback);
        },
        generateQuestionList: function(themeNames, callback) {
            ThemeService.findIdsByNames(themeNames, themeIds => {
                QuestionRepository.findByThemesWithAnswers(themeIds, questions => {
                    shuffle(questions);
                    callback(questions);
                });
            });
        },
        findById: function(id, callback) {
            QuestionRepository.findById(id, callback);
        },
        findByTheme: function (themeId, callback) {
            QuestionRepository.findByTheme(themeId, callback);
        },
        save: function (title, theme, answers, userId, isHistory, callback) {
            if (title.length < 255) {
                ThemeService.findIdsByNames([theme],function(themes){
                    let themeId = themes[0];
                    const question = new Question(null, title, themeId, isHistory, userId);
                    let qId = undefined;
                    QuestionRepository.save(question, (res) => {
                        qId = res.insertId;
                        let answersToAdd = [];
                        answers.forEach((answer) => {
                            if (answer.title.length > 255) {
                                callback(exceptions.FUNCTIONAL_EXCEPTION.TOO_MUCH_CHARACTERS);
                            } else {
                                answersToAdd.push(new Answer(null, answer.title, qId, answer.correct ? answer.correct = 1 : answer.correct = 0));
                            }
                        });
                        AnswerRepository.save(answersToAdd, (res) => {
                            if (res) callback(true);
                        });
                    });
                });

            } else {
                callback(exceptions.FUNCTIONAL_EXCEPTION.TOO_MUCH_CHARACTERS);
            }
        },
        delete: function (id, callback) {
            QuestionRepository.delete(id, (res) => {
                callback(res);
            });

        }
    };
};
