const logger = require("../logger");
const Game = require("../model/Game");
const RoomService = require("./RoomService")();
const ThemeService = require("./ThemeService")();
const Question = require("../model/Question");
const Answer = require("../model/Answer");
const configuration = require('properties-reader')('configuration.properties');
const QuestionService = require("./QuestionService")();
const exceptions = require("../exceptions");
const messages = require("../game/messages");

module.exports = function () {

    this.games = [];

    function findGameByHostClientId(hostClientId) {
        return games.find(game => {
            if (game.host === undefined) {
                return undefined;
            }
            return game.host.clientId === hostClientId
        });
    }

    function findGameByPlayerClientId(playerClientId) {
        return games.find(function (game) {
            const clientId = game.room.playerIdList.find(function (clientId0) {
                return clientId0 === playerClientId;

            });
            return clientId !== undefined;
        });
    }
    function nextQuestion(game) {
        const question = game.nextQuestion();
        if (question === undefined) {
            logger.info(`Ending game ${game.room.roomCode}`);
            const results = game.getResults();
            logger.debug("Results: " + JSON.stringify(results));
            game.sendMessageToHost(messages.MESSAGES.END_GAME_HOST, {results: results});
            game.sendPersonalizedMessageToPlayers(messages.MESSAGES.END_GAME_PLAYER, function(player) {
                const result = results.find(r => r.playerClientId === player.clientId);
                return {
                    score: result.score,
                    rank: result.rank
                }
            });
            game.end();
        } else {
            logger.info("new question: " + question.title);
            game.sendMessageToHost(messages.MESSAGES.DISPLAY_QUESTION, {
                question: question.title,
                answers: question.answers.map(answer => answer.title)
            });
            game.sendMessageToPlayers(messages.MESSAGES.ASK_FOR_ANSWER, {
                answerCount: question.answers.length
            });
        }
    }

    return {
        hostGame: function (socket, hostClientId) {
            const game = new Game(RoomService.findRoomByHostClientId(hostClientId));
            game.connectHost(socket, hostClientId);
            games.push(game);
            logger.info("Host " + hostClientId + " connected to game " + game.room.roomCode);
        },
        joinGame: function (socket, playerClientId, playerName) {
            logger.trace("playerClientId: " + playerClientId);
            const game = findGameByPlayerClientId(playerClientId);
            game.connectPlayer(socket, playerClientId, playerName);
            return game;
        },
        startGame: function (socket, hostClientId, themeNames, callback) {
            const game = findGameByHostClientId(hostClientId);

            // generate question list
            game.themes = themeNames;
            QuestionService.generateQuestionList(themeNames, questions => {
                game.questions = questions.slice(0, questions.length < 5 ? questions.length : 5);
                logger.info(`Game ${game.room.roomCode} started with themes ${themeNames}`);
                logger.trace("Question list: " + game.questions);
                callback(game);
            });
        },
        nextQuestion: function (game) {
            nextQuestion(game);
        },
        giveAnswer: function (clientId, answer, callback) {
            logger.trace(`Player ${clientId} is giving answer "${answer}"`);
            const game = findGameByPlayerClientId(clientId);
            game.setPlayerAnswer(clientId, answer);
            logger.debug(`Player ${clientId} gave answer "${answer}"`);
            callback(game);
        },
        setPlayerReady: function(clientId) {
            const game = findGameByPlayerClientId(clientId);
            const mode = configuration.path().mode;
            logger.trace(`mode: ${mode}`);
            game.setPlayerReady(clientId);
            if (game.allPlayersReady()) {
                game.sendMessageToHost(messages.MESSAGES.ALL_PLAYERS_READY, {});
                game.resetPlayersReady();
                setTimeout(function() {
                    nextQuestion(game);
                }, mode !== 'test' ? 3000 : 0);
            }
        },
        addQuestion: function(clientId, title, answers) {
            logger.info(`Client ${clientId} added question ${title}`);
            const game = findGameByPlayerClientId(clientId);
            const question = new Question(undefined, title, undefined, 1, -1, answers.map(
                answer => new Answer(undefined, answer.title, undefined,  answer.correct)
            ));
            game.queueQuestion(question);
        }

    };
};
