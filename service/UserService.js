const logger = require("../logger");
const exceptions = require("../exceptions");
const UserRepository = require("../repository/UserRepository");
const User = require('../model/User');
const bcrypt = require('bcrypt');

const regExpMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


module.exports = function () {
    let connectedUsers = [];
    return {
        findAll: function(callback) {
            UserRepository.findAll(callback);
        },
        findById: function(id, callback) {
            UserRepository.findById(id, callback);
        },
        findByUserName: function(userName, callback) {
            UserRepository.findByUserName(userName, callback);
        },
        findByMail: function (mail, callback) {
            UserRepository.findByMail(mail, callback);
        },
        connectUser: function (mail, password, callback) {
            UserRepository.findByMail(mail, (user) => {
                const connectedUser = connectedUsers.find(function (users) {
                    if (user !== undefined) return users.id === user.id;
                });
                if (user === undefined) {
                    callback(exceptions.FUNCTIONAL_EXCEPTION.USER_NOT_FOUND);
                } else {
                    bcrypt.compare(password, user.password, (err, res) => {
                        if (res === true) {
                            if (connectedUser === undefined) {
                                user.connected = true;
                                connectedUsers.push(user);
                                logger.info("Users connected : " + JSON.stringify(connectedUsers));
                                callback(new User(user.id, user.mail, null, user.userName, user.avatar, user.connected));
                            } else {
                                callback(exceptions.FUNCTIONAL_EXCEPTION.USER_ALREADY_CONNECTED);
                            }
                        } else {
                            callback(exceptions.FUNCTIONAL_EXCEPTION.USER_ALREADY_CONNECTED);
                        }
                    });
                }
            });
        },
        signUp: function (mail, password, userName, avatar = null, callback) {
            logger.info("POST /User");
            if (regExpMail.test(mail) && userName.length < 20) {
                UserRepository.findByMail(mail, function (response) {
                    if (response) {
                        callback(exceptions.FUNCTIONAL_EXCEPTION.USER_ALREADY_EXISTS);
                    } else {
                        bcrypt.hash(password, 10, (err, hash) => {
                            const user = new User(null, mail, hash, userName, avatar);
                            UserRepository.save(user, (res) => {
                                callback(res);
                            });
                        });
                    }
                });
            } else {
                callback(exceptions.FUNCTIONAL_EXCEPTION.INVALID_CREDENTIALS);
            }
        },
        disconnect: function (id, callback) {
            const userToDelete = connectedUsers.find(function (users) {
                return parseInt(users.id) === parseInt(id);
            });
            if (userToDelete === undefined) {
                callback(exceptions.FUNCTIONAL_EXCEPTION.USER_NOT_FOUND);
            } else {
                logger.debug("userToDelete : " + JSON.stringify(userToDelete));
                connectedUsers.splice(connectedUsers.indexOf(userToDelete), 1);
                logger.info("Users connected : " + JSON.stringify(connectedUsers));
                callback(true);
            }
        },
        updateUser: function (id, mail=null, oldPassword=null, newPassword=null, userName=null, avatar = null, callback) {
            const userToUpdate = connectedUsers.find(function (users) {
                return users.id === parseInt(id);
            });
            if (userToUpdate) {
                if ((mail && regExpMail.test(mail)) || !mail) {
                    if ((userName && userName.length < 20) || !userName) {

                        if (mail) userToUpdate.mail = mail;
                        if (userName) userToUpdate.userName = userName;
                        if (avatar) userToUpdate.avatar = avatar;
                        if (oldPassword && newPassword) {
                            bcrypt.hash(oldPassword, 10, (err, hash) => {
                                if (bcrypt.compare(userToUpdate.password, hash)) {
                                    bcrypt.hash(newPassword, 10, (err, hashPassword) => {
                                        userToUpdate.password = hashPassword;
                                        UserRepository.update(userToUpdate, (res) => {
                                            callback(res);
                                        });
                                    })
                                } else {
                                    return exceptions.FUNCTIONAL_EXCEPTION.INCORRECT_PASSWORD;
                                }
                            })
                        }
                        else {
                            UserRepository.update(userToUpdate, (res) => {
                                callback(res);
                            });
                        }
                    } else {
                        callback(exceptions.FUNCTIONAL_EXCEPTION.INCORRECT_USERNAME)
                    }
                } else {
                    callback(exceptions.FUNCTIONAL_EXCEPTION.INCORRECT_MAIL_ADDRESS)
                }
            } else {
                callback(exceptions.FUNCTIONAL_EXCEPTION.USER_NOT_FOUND)
            }
        }
    };
};
