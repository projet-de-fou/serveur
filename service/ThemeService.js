const logger = require("../logger");
const exceptions = require("../exceptions");
const ThemeRepository = require("../repository/ThemeRepository");

module.exports = function () {
    return {
        findAll: function(callback) {
            ThemeRepository.findAll(callback);
        },
        findByNames: function(names, callback) {
            ThemeRepository.findByNames(names, callback);
        },
        findById: function(id, callback) {
            ThemeRepository.findById(id, callback);
        },
        findIdsByNames: function(names, callback) {
            this.findByNames(names, (themes) => callback(themes.map(theme => theme.id)));
        }
    };
};